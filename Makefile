# Dependencies:
# * build-essential (make)
# * texlive-fonts-extra
# * texlive-lang-portuguese
# * wget

current_dir = $(shell pwd)

all: clean setup build

setup:
	wget http://mirrors.ctan.org/macros/latex/contrib/mwcls.zip -O mwcls.zip; \
	unzip mwcls.zip; \
	rm mwcls.zip; \
	cd mwcls ; latex mwcls.ins ; cd -; \
	ln -s mwcls/mwbk10.clo .; \
	ln -s mwcls/mwbk.cls .; \
	ln -s mwcls/mwbk.ins .; \
	wget http://mirrors.ctan.org/macros/latex/contrib/br-lex.zip -O br-lex.zip; \
	unzip br-lex.zip; \
	rm br-lex.zip; \
	ln -s br-lex/br-lex.cls .

build:
	pdflatex -synctex=1 -interaction=nonstopmode -file-line-error -recorder -output-directory=$(current_dir) $(current_dir)/estatuto.tex
	pdflatex -synctex=1 -interaction=nonstopmode -file-line-error -recorder -output-directory=$(current_dir) $(current_dir)/estatuto.tex

clean:
	rm -rf br-lex* mw* *.pdf *.aux *.fls *.log *.out *.gz *latexmk