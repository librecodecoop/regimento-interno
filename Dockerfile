FROM debian

RUN apt-get update && \
    apt-get install -y -q \
        build-essential \
        texlive-fonts-extra \
        texlive-lang-portuguese \
        wget

WORKDIR /latex